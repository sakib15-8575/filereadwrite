import java.io.*;
import java.util.Scanner;

public class FileReadWrite {
    public static void main(String[] args) {

        String fileToRead = args[0];
        String fileToWrite;
        int read;
        String text = "";

        Scanner scan = new Scanner(System.in);
        System.out.print("File path for writing = ");
        fileToWrite = scan.nextLine();

//       read file = /home/sakib/Desktop/readFile (image_c.jpg)
//       write file = /home/sakib/Pictures/writeFile

        File file1 = new File(fileToRead);
        File file2 = new File(fileToWrite);

        FileReader fr = null;
        FileWriter fw = null;

        try {
            fr = new FileReader(file1);

            while ((read = fr.read()) !=-1) {
                text = text +" "+(byte) read;
            }
            
            System.out.println("File Reading Done");

            fw = new FileWriter(file2);
            fw.write(text);
            
            System.out.println("File Writng Done");

            fw.close();

        }catch (FileNotFoundException ex){
            System.out.println(ex.getMessage());
        }catch (IOException ix){
            System.out.println(ix.getMessage());
        }
    }
}

